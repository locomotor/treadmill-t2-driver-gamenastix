# Gamenastix tracker driver for SteamVR #

This README would normally document whatever steps are necessary to get your application up and running.

### Dependencies

* Windows SDK 10.0.17134.0
* Test Adapter for Google Test (form Visual Studio Installer)

### What is this repository for? ###

* Simulate SteamVr trackers

### How to test? ###

To test, you must copy the entire ./bin/drivers/gamenastix directory to C:\Program Files (x86)\Steam\steamapps\common\SteamVR\drivers\.
It also contains all required configuration files for SteamVR.

NuGet packages (automated installed when build - https://docs.microsoft.com/en-us/nuget/consume-packages/package-restore):

* Microsoft.googletest.v140.windesktop.msvcstl.static.rt-dyn by Microsoft
* glm by GLM contributors