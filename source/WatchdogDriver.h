// This file is a part of Treadmill project.
// Copyright 2018 Disco WTMH S.A.

#pragma once

#include <openvr_driver.h>


class WatchdogDriver : public vr::IVRWatchdogProvider
{
public:
    WatchdogDriver() = default;

    ~WatchdogDriver() = default;

    vr::EVRInitError Init(vr::IVRDriverContext *driverContext) override;

    void Cleanup() override;
};
